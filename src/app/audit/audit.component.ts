import { Component, OnInit, ViewChild } from '@angular/core';
import { first } from 'rxjs/operators';
import { MatTableDataSource } from '@angular/material/table';
import { Audit } from '@/_models';
import { AuditService, AuthenticationService } from '@/_services';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';



export interface UserData {
  course: string;
  name_of_batch: string;
  scheduled_date: string;
  scheduled_enddate: string;
  uid: string;
  // class_no: string;
  // price: number;

}


@Component({ templateUrl: 'audit.component.html' })
export class AuditComponent implements OnInit {

  certificatesArray = []
  displayedColumns: string[] = ['ip', 'id', 'user', 'loginTime', 'logoutTime'];
  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  audits = [];

  constructor(
    private authenticationService: AuthenticationService,
    private auditService: AuditService
  ) {
  }

  ngOnInit() {
    this.loadAllAudits();
    setTimeout(() => {
    this.onChange(12)
    }, 100);
  }

  private loadAllAudits() {
    this.auditService.getAll()
      .pipe(first())
      .subscribe(audits => {
        this.audits = audits
        this.dataSource = new MatTableDataSource(this.audits);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      })



  }

  dateppp = 'dd/MM/yyyy hh:mm:ss'
  onChange(event) {
    if (event == 12) {
      this.dateppp = 'dd/MM/yyyy hh.mm.ss'
    } else {
      this.dateppp = 'dd/MM/yyyy HH.mm.ss'
    }
  }




  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }




}